import React from "react";
import NavBar from "../NavBar";

const Home = () => {
  return (
    <div>
      <NavBar />
      <div className="main">
        <h1>Welcome to Mysite!!</h1>
        <h4>learning about react-router & get started...</h4>
      </div>
    </div>
  );
};
export default Home;
