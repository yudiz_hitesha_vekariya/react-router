import React from "react";
import Data from "./Start/Data";
import "../Pages/Start/Data.css";
import NavBar from "../NavBar";

const Getstarted = () => {
  return (
    <div>
      <NavBar />
      <div className="todo">
        {Data.map((val, index) => {
          return (
            <div key={index} className="cards">
              <div className="card">
                <img src={val.imgsrc} alt="" />
                <div className="card_info">
                  <span className="food_name">{val.titles} </span>
                  <h3 className="card_title">{val.sname} </h3>
                  <a href={val.link} target="blank">
                    <button className="btn" type="submit">
                      More Info
                    </button>
                  </a>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
export default Getstarted;
