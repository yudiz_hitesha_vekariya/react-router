import React from "react";
import NavBar from "../NavBar";
import "../Pages/About.css";

const About = () => {
  return (
    <div>
      <NavBar />
      <div className="about">
        <h1 className="title">React Router</h1>

        <h5 className="partsof">
          <i class="fa-solid fa-star"></i>
          what is react-router?
        </h5>

        <p className="para">
          React Router is the standard routing library for React. From the docs:
          “React Router keeps your UI in sync with the URL. It has a simple API
          with powerful features like lazy code loading, dynamic route matching,
          and location transition handling built right in.
        </p>
        <br></br>
        <h5 className="partof">
          <img
            className="img12"
            src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDw8PDg8QDw0ODw8PDw8NDw8PDQ0OFREWFhURFRUYHSggGBolHRUVITEhJikrLi4uFx8zODMtNygtLisBCgoKDg0OFhAQGy0lHx4rLSstLTArLS0vKystLS0tKy0wLS0tLSsrKystKy0tLS0tLS0tLS0rLi0tLS0tLS0tLf/AABEIAM0A9gMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAAAwIEBQEHBgj/xABDEAACAgEBBAYFBwwBBAMAAAABAgADEQQFEiExBhNBUWGBIjJxkaEHUmKiseHiFCNCQ1NygqOywcLRM5Kzw/AWc4P/xAAXAQEBAQEAAAAAAAAAAAAAAAACAAED/8QAIREBAQACAgMAAwEBAAAAAAAAAAECERIhAzFBIjJh8cH/2gAMAwEAAhEDEQA/APDYQhJCEISQhCEkIQhJCE7idCySM7iTCRyUzdM2rhZIVS6mnj1003Q3Jmimd6mao007+TS0zmyDTImszXOmin08tLmyys5iXn08Q9UzRTJXhJlZzEwkYTuJySEIQkhCEJIQhCSEIQkhCEJIQhCSEJ0CSAkkQJILJBZMLN0zaAWTVIxUlmmmbobS6qJcq08fRRPpejOx+vtUYzxAjmO3LLNmaDYNtnqqfdNB+jFyjJU+6e8bN2LpNn0B7gm9wyzDIDH9ECPr2js+/wBEhOPD0kC/ETdz5Gcb9un51s2cy8GXEj+ST3fbvQmq1S9GM4zu88+wz4zS9DbGt3Qp58scopJe455csbqvP6tlO/qrLJ6LXEZ3T7p7psnolptMu9dukjnk4Qf7ll9tbPRhVhOIPKsbuBjP2iZufJs+N+3T82a/Y9lXrKfdMq3Tz9LdLOi+n1NDXUKoO7vej6rL3junhG2NndXYy47ZmpZuN3cbqvlLaZXZJtXUSjdVBY6TJRKyJEeyyBWHR7JxORhEiRMajCEJNEIQkhCEJIQhCSE6BACTAkgBJqsFEYqxSC4FjFWSVY1EmyDa7VXL+npi9PXNTTVRyOeVT09E9J+TDRg3oSORz7uM+I01M9H+Thd25OHePeMTpJ1XHf5Rs/KU5ZBUCQCh4jmCcjP2T4HZN9jIj8mI9IA+q44MvkQR5T0vploGtZMDPo4+Jnz+xuilqtYChCG0uvokDDgM31y585uE/GLybuVbXRLX3khOLL2g9nj4T68oBlgo3iPYW8Myps3QLp68KMvjjy4nuiqV1ItLMMqeY3l5e+cstZW2f674bwkl3f8Aj4/pTtG9mIOQBkbvICfDaxmF1BJPpM9fmUL/APjM9l25shb13gPT+2fC7Y6L2gBgjZrsSwcD6oOG+qWnXGzKdOGeOWOXfb67oVaX0xRuIBxx7iOU8k6baMLc2OxjPYeiWmNVLZ7SPgPvnmHTKstaxx+kYZPyyLK/hg881FEzdRTPo9TTMrU1Q2NxyYVtcSyzRvrlRlgsdpVVliyJZZYtlhsOUgiRjSJAiFqEJ2ck0QhCSE6JySEk6BJqJxRGKJsFJRGqs4gjVEUGuqsfUkiqyzSscg2rOmrmxodOWIAHGUNMk9F+T7YYvsXe5cye4TpJ9cMu+otdGOiFl2CVwvax5Ceh7N2FRpQGLekO0ndGfDvnNs7Wr0SCusANjgOxR3nxnx1m2rLX3mYnj3ynLP8AkV4+P+16Nq9SK038b3LGPGc0eoNi75XdXs45PtlTZdgv04U8wMH7QZHbOsFK1VLw61tz2AIzf4zlxn6/du/O/v8ANIanarbxFeMDwzIHadvh7obN0m/xPIc+/wBk0zoa8YxjxyczplcMbrTnjPJlN7VNn7SLtuvgE8jy4x+u1ppIyu8rdoOCDMvW0GtvZxB7xNOhl1FWDzBGfAjtmZY4zWWum45ZauO+1gWLubx9FWGTnxmDtXotTqAWrbieWeKnzEl0x1rU0AVjeI9JkHrMg7F8e3HbjHbPk9l9JXTddH3q2wRg5VhLDC63KzyZ4745Tb5/pL0XsoJypH2ET4jV04yDP0ZTbTtCgggZxxHard48J4v0y2X1FrDGMEgxS79+4Fx46s9V8LqElCxZr6lJnWrBY641TZYpllp1iWEFOVWYRZEsMIphDThJEjGESBha5CEJNEmJESYkxNRGqItY1IoNNURyCLSOQRwaYglqkRCCWqY4FaGmE9c+S3VKG3TjLKQPb/6J5Lpp9L0f2g1Lgg4wY9bmnK3VlekdN9mWNZ1gyVYDyIHKfM6aoggET7rYfSSnVKa7sbwCg59Vsj4GO1XRutjv1EDtweXkZY58fxyWfj5flgd0ZpK1bx7f7Sn0qpYNTZzAuq8sncP9U19Qpqo3VHEADh8ZCsDU0br+sCp/iUgg/CCZXlz+OlxnGeP77c2NaCCvbzHiJpT5Ylq2I4gg9nMR7bSsIxvHHkD74s/DcruMw80k1TdqapHYhCG3Ca2x2OOa+WZa2JQVQsf0uXs75g7D0zPZcpHD8ods/RKqf7zf2pqOrVa0HFh2dgGJZTqeOMxu7fJfjJ6YUng3YR7iJ5xdoXSwtSCyOxNtI+cedlfcx7V5HnwOd72DWabr6ACPSIB48OPbKWj2DTT6dpBI48eCj/csfJJjq+4zPxZXPc9VT6D6Bq6zY3quMLnhnxnnvyj6lbL7CvLeM+26W9KxVWyaficYPZvL81e6eR7S1vXnrAcq3EZyD5g8j4Sxl3cr9WVmpjPjC1ImbcJqamZ1omUsVRxEOJZcRLiCukVnEUwj2ES0FOEtIGMaQMNJCEITGuiTWQEmsmGrGpFLGpHBp6RyRCR6RQKfXLNUrJLFZjg1o6czV0zTGoaaenedI5ZRtbK1TJbbg/s/6Z6N0R2nbY6pvHBPEZ4Ynlehf85b/wDn/TPRugDg2r5/YYr+tCfvH2+0tpigqCM5GeeIqrbtBxzBJA7OZOBMbpoxDL+6PtM+R1OpI6kA+tfWPdlv8Zzw8WNxlrpn5c5nZHqtlVbDfZQwxnOM8JWV9MTgAE9262Zl9Htqk4RskHke4zaTR1ozWYx2+C98Fx4XV3/HXHLnJcZP6mRXSpbAUduBxMzNV0jorKjBJdt0chx3Wb7FMwekm2mNyIOFbrYo/wDsXBA9pXfP8M+X21eer3weNTpd3ndRgXA8Sm8POPHwzW8vbll5rvWPp6ps/WdchYDHHHf2Tzfpftq1HQljui0K/PgrgqPrFfLM+16HnNTd2Rj4zz3piFayxSMq28pHeDwM3CSZZSM8mVuGNrB115bOTPl7NUEstQqwU2ZVsAplkUsOHLiSfOai3txrc5srx6Rx+dTss8+3xB7MTMZ8vcPpL/21m5VmM0Re0o2S1cZUcwV0hDxDxzxDwV0hLxLRzxLQUoU0W0Y0W0NNCEITGuiTEXJCSOUxqxCmNUxQasIY5DK6GNQxwKtIZYQyohjkaKBV6ppfosmUjSzVZHKFjX0dv5yz2J9hn23Q3aQruQnsI90870135x/3a/8AKa+j1hQggxyueU7e6dINm/lVatXgkDh9JTPmKeiN7vUWG6K7C5LcP1br/lM7YHTZqlCsQy9zdk1NT8oPpBEVQSjtniSMFR/l8IJM8ZqHb48ru+31Wl0VOkTeYjPef7CVquktZs3TwQ8Ae0eM8+2j0ksuJyxPnM1dcQc54xTxy/t3Rvls6w6j1LbOwKtUgeoqHDB0I9XeH2ZBIPgTPn7ui9+9jcyPIjExtl9K7KeTHHceIM23+UHdXPVq2OeM73tAzx9kzWePrtu/Hl76fR6DTroNHuseKKce7CrntwMTxzpDrrOtbh1qZOCrBbR4EHgfbkeybHSPpgdYnov6OTums4KN/vvB858Rfq7QTvqLPpVkK3mrH7D5SxnH37qyvKyT1CdbqVYjGVuUncFgKbxxxTJ4EEd2eQPZM+q8M9rDkWU8ef8Axpzj9Rq0bKtwB4YsUqCfDPA+Uy9Od17FyCd4twIJwcYJx29/jDb2WM6WbGldzJu0Q7TKcLcxLmTcxLGClEHMS0m5imMNOINFmTYxZhpOQhCY0TonISRgMYpiQZNTNjFhTHK0rKYxWigWLStHK0qK0arRSjYuI8ejyirRyvFKNi3Tb+cb9yv+p5fqvmMr+n7V+w/fLK2xSjY269RO1anNrdy1oB7Sz5/pEyUvhRqPSsP0gB7Ai/3Ji2PF9CNTJflMxRqJ38oi2PFrtqYl9T4zMOoi21EzkuKzqN0ne4h/nISrHuBI5jwOZSsvcfphv3xusf4l4fCRe6Jd8w2nI5ZqM8GG7nhngUPhn/eJUNYVxjmUJ+sRiTsHPHI8weIMphyH3WPpLvKeIbgeI4jnBaelx2iWaDNEs0y0pAzRTGDNFMYbSkcYxbGdYxZMNKOEyMCZyEhCEJIQhCSdkgZCSCyRimMVotU8Y1avH4RQamrRitIpp/H4Ry6X6Xw++KbG6CvGq86miPzvq/fHJs8/P+r98UlG2E7/AKSn94e/B/tHCyTs2aQAd/ky/o8gTgnn2AmWl2Of2n1PxTZKNsVRZOUWcD4s39RAmimwif1v8v8AFDR7BL1o3W43lDf8eeYz86LjkzcU+th1s1R0ab9t/K/FO/8Axlv238r8U3jkzcZJtkTZNZujbftv5X4otuj5H63+X+KXHJu4yjZIF5pPsQj9b/L/ABRL7II/WfU++HjW7jPZ5nWH0mYcww92JtPsw/P+p98ybqcPYCeTADhzyOfPwMGUp42GF8xbNJVac8BvcCCeXIZ9sk2m+l8PvmdrpXZotmjmo8fhFNV4/CGlCiZEmTZPGLIhJyEISaIQhJCEISTojFixJrJhyRyRCGPQxwasJLFcrIZYQxwKtVy1WJUrMs1tHHOrQrDAqeTAg+wiWNExZRn1h6LdnpA4Pl2+cr1tJb/VsX/QbHWfRI4Cz3cD4Ad0bGvSOUlshfzFPhVWD7QoERVZJaC7dLVnmrMy/SrYkgjwGd3+HxjntjZrQRrVCUk1EmdTHtiVqCU7VjLL5WssmVEWiVLBLFjyrY0FStYJgbVQLYrZxkjjjhjh9hGf4pt33qvrMo9pAmRtLUVOuN8ZByMZP2Tjn6dcPZVVnpYPDgR55zGPMzLdgYgciAcjwjl1T49JCT34InOZHcTXiHnW1GewjyMU9vgfdMtbEXimkmfwkCYaaMIQmNEIQkhCEJISamQnRJHKY5DKymMVopRsW0aWEaUkaOR4pQsXkeWEeZ6PHJZHKFjSSyWUtmWlsatsco2L6Ar/AMbbo7UYZTy+b5cPCSsuLAb9ZJB4GqwZXxBJUjylRbpMXTdsWV1Ng9V39lqKwH/Tg/GTXW3dprPsR1/yMqi4TvXCW0snX2dqg+w4le3V6hvVVFHeSzH+wnDfFtfLbJiS9epb1tRujuRVHxlHX6E7oJsschs+mxIxNBromyzMFkOWkVaOnAO4M+PGSIUcgB7ABIl8RT2TOmuu8Q7Qd4h3htKQO0SxgzRTNDacjjGLJnSZGAhCEJNEIQkhCEJIQhCSdBkg0hCSPVoxXlYGSDTdjpcWyNWyUQ8mtkW2WNBbIxbZniyTFsWx00RdJC6ZwtkhbN5M4tHroddM/rZ3rZcmcV83SJulHrZE2y5LiuNdINbKhtkTZM23isNZFNZEmyLLzNlIczxTPFl5AtDstJM0gTOEzkLRCEJNEIQkhCEJIQhCSEIQkhCEJIQhCSdzOhpGEkYHkg8TCW2aPFk71kRmGZu1pY6yHWSvmG9La0sdZOdZEZhmW1o4vIl4vM5La0mWnC0jCY13M5CEkIQhJCEISQhCEkIQhJP/2Q=="
            alt=""
          />
        </h5>
        <br></br>
        <h5 className="partsof">
          {" "}
          <i class="fa-solid fa-star"></i>how we can install react-router-dom?
        </h5>

        <p className="para">npm i react react-dom react-router@2.0.1 --save</p>
        <br></br>
        <h5 className="partsof">
          {" "}
          <i class="fa-solid fa-star"></i>The React Router API: Router, Link,
          and Route
        </h5>
        <p className="para">
          The React Router API is based on three components:
          <br></br>
          <h5 className="partsof1"> Routers: </h5> The routerthat keeps the UI
          in sync with the URL
          <br></br>
          <h5 className="partsof1"> Link: </h5> Renders a navigation
          <br></br>
          <h5 className="partsof1"> Link route:</h5> Renders a UI component
          depending on the URL
        </p>
        <br></br>
        <h5 className="partsof">
          {" "}
          <i class="fa-solid fa-star"></i>Keywords
        </h5>
        <p className="par">react router route routing history link </p>
        <br></br>
        <h5 className="partsof">
          {" "}
          <i class="fa-solid fa-star"></i>Definations of main-concepts
        </h5>
        <br></br>
        <p className="para">
          <h5 className="partsof1">URL -</h5>
          The URL in the address bar. A lot of people use the term "URL" and
          "route" interchangeably, but this is not a route in React Router, it's
          just a URL.
          <br></br>
          <h5 className="partsof1"> Location -</h5>This is a React Router
          specific object that is based on the built-in browser's
          window.location object. It represents "where the user is at". It's
          mostly an object representation of the URL but has a bit more to it
          than that.
          <br></br>
          <h5 className="partsof1"> Location State -</h5> A value that persists
          with a location that isn't encoded in the URL. Much like hash or
          search params (data encoded in the URL), but stored invisibly in the
          browser's memory.
          <br></br>
          <h5 className="partsof1">History Stack - </h5>As the user navigates,
          the browser keeps track of each location in a stack. If you click and
          hold the back button in a browser you can see the browser's history
          stack right there.
          <br></br>
          <h5 className="partsof1">Client Side Routing (CSR) - </h5>A plain HTML
          document can link to other documents and the browser handles the
          history stack itself. Client Side Routing enables developers to
          manipulate the browser history stack without making a document request
          to the server.
          <br></br>
          <h5 className="partsof1"> History - </h5>An object that allows React
          Router to subscribe to changes in the URL as well as providing APIs to
          manipulate the browser history stack programmatically.
          <br></br>
          <h5 className="partsof1"> History Action - </h5>One of POP, PUSH, or
          REPLACE. Users can arrive at a URL for one of these three reasons. A
          push when a new entry is added to the history stack (typically a link
          click or the programmer forced a navigation). A replace is similar
          except it replaces the current entry on the stack instead of pushing a
          new one. Finally, a pop happens when the user clicks the back or
          forward buttons in the browser chrome.
          <br></br>
          <h5 className="partsof1"> Segment - </h5>The parts of a URL or path
          pattern between the / characters. For example, "/users/123" has two
          segments.
          <br></br>
          <h5 className="partsof1">Path Pattern -</h5>These look like URLs but
          can have special characters for matching URLs to routes, like dynamic
          segments ("/users/:userId") or star segments ("/docs/*"). They aren't
          URLs, they're patterns that React Router will match.
          <br></br>
          <h5 className="partsof1">Dynamic Segment - </h5>A segment of a path
          pattern that is dynamic, meaning it can match any values in the
          segment. For example the pattern /users/:userId will match URLs like
          /users/123 URL Params - The parsed values from the URL that matched a
          dynamic segment.
          <br></br>
          <h5 className="partsof1">Router -</h5> Stateful, top-level component
          that makes all the other components and hooks work.
          <br></br>
          <h5 className="partsof1">Route Config -</h5> A tree of routes objects
          that will be ranked and matched (with nesting) against the current
          location to create a branch of route matches.
          <br></br>
          <h5 className="partsof1">Route - </h5>An object or Route Element
          typically with a shape of path, element or Route path element. The
          path is a path pattern. When the path pattern matches the current URL,
          the element will be rendered.
          <br></br>
          <h5 className="partsof1"> Route Element - </h5>Or Route. This
          element's props are read to create a route by Routes, but otherwise
          does nothing.
          <br></br>
          <h5 className="partsof1"> Nested Routes - </h5>Because routes can have
          children and each route defines a portion of the URL through segments,
          a single URL can match multiple routes in a nested "branch" of the
          tree. This enables automatic layout nesting through outlet, relative
          links, and more.
          <br></br>
          <h5 className="partsof1"> Relative links - </h5>Links that don't start
          with / will inherit the closest route in which they are rendered. This
          makes it easy to link to deeper URLs without having to know and build
          up the entire path.
          <br></br>
          <h5 className="partsof1">Match -</h5> An object that holds information
          when a route matches the URL, like the url params and pathname that
          matched.
          <br></br>
          <h5 className="partsof1"> Matches - </h5>An array of routes (or branch
          of the route config) that matches the current location. This structure
          enables nested routes.
          <br></br>
          <h5 className="partsof1">Parent Route -</h5> A route with child
          routes.
          <br></br>
          <h5 className="partsof1">Outlet -</h5> A component that renders the
          next match in a set of matches.
          <br></br>
          <h5 className="partsof1"> Index Route - </h5> child route with no path
          that renders in the parent's outlet at the parent's URL.
          <br></br>
          <h5 className="partsof1"> Layout Route -</h5> A parent route without a
          path, used exclusively for grouping child routes inside a specific
          layout.
        </p>
      </div>
    </div>
  );
};
export default About;
