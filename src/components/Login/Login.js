import React from "react";
import "../Login/Login.css"

function Login(props) {
  const onSubmit = () => {
    localStorage.setItem("token", "randomValue");
    props.history.push("/home");
  };
  return (
    <div className="login">
      <div className="tostar">
        <h1 className="tit">LOGIN</h1>
        <br></br>
        <h3 className="label">UserName</h3>
        <div>
          <input type="username" placeholder=" your username"></input>
        </div>
        <div>
          <h3 className="label">PASSWORD </h3>
          <div>
            <input type="password" placeholder="Your password"></input>
          </div>
        </div>
        <button onClick={onSubmit}>submit</button>
      </div>
    </div>
  );
}

export default Login;
