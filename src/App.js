import "./App.css";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./components/Login/Login";
import Home from "./components/Pages/Home";
import About from "./components/Pages/About";
import Getstarted from "./components/Pages/Getstarted";
import Logout from "./components/Pages/Logout";

const PrivateRoute = (props) => {
  const token = localStorage.getItem("token");
  if (token) {
    return <Route exact={true} path={props.path} component={props.component} />;
  } else {
    return <Login {...props} />;
  }
};

function App() {
  return (
    <>
      <Router>
        <div className="pages">
          <Switch>
            <Route path="/" component={Login} exact />

            <PrivateRoute exact path="/home" component={Home} />
            <PrivateRoute exact path="/about" component={About} />
            <PrivateRoute exact path="/getstarted" component={Getstarted} />
            < PrivateRoute exact path="/logout" component={Logout} />
          </Switch>
        </div>
      </Router>
    </>
  );
}

export default App;
